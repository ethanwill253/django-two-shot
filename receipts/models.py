from django.conf import settings
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

class Receipts(models.Model):
  vendor = models.CharField(max_length=100)
  total = models.DecimalField(decimal_places=2, max_digits=12)
  tax = models.DecimalField(decimal_places=3, max_digits=12)
  date = models.DateTimeField()
  category = models.ForeignKey(
    'Categories',
    related_name="receipts",
    on_delete=models.CASCADE,
    null=True
    )
  account = models.ForeignKey(
    "Accounts",
    null=True,
    related_name="receipts",
    on_delete=models.CASCADE,
    )
  purchaser = models.ForeignKey(
    settings.AUTH_USER_MODEL,
    related_name="receipts",
    on_delete=models.CASCADE,
    null=True
    )



  def __str__(self):
      return self.name


class Accounts(models.Model):
  name = models.CharField(max_length=100)
  number = models.PositiveIntegerField()
  owner = models.ForeignKey(
    settings.AUTH_USER_MODEL,
    related_name="accounts",
    on_delete=models.CASCADE,
    null=True
    )


  def __str__(self):
      return self.name


class Categories(models.Model):
  name = models.CharField(max_length=100)
  owner = models.ForeignKey(
    settings.AUTH_USER_MODEL,
    related_name="categories",
    on_delete=models.CASCADE,
    null=True
  )

  def __str__(self):
      return self.name


# Create your models here.
