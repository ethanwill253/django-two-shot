from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
# Create your views here.
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.models import Receipts, Accounts, Categories

class ReceiptsListView(LoginRequiredMixin, ListView):
  model = Receipts
  template_name = 'receipts/list.html'


class ReceiptsCreateView(CreateView):
  model = Receipts
  template_name = 'receipts/create.html'
  fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']
  success_url = reverse_lazy("receipt_list")

  def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class AccountsListView(ListView):
  model = Accounts
  template_name = 'accounts/list.html'


class AccountsCreateView(CreateView):
  model = Accounts
  template_name = 'accounts/create.html'
  fields = ['name', 'number']
  success_url = reverse_lazy("accounts_list")

  def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class CategoriesListView(ListView):
  model = Categories
  template_name = 'expense_categories/list.html'


class CategoriesCreateView(CreateView):
  model = Categories
  template_name = 'expense_categories/create.html'
  fields = ['name']
  success_url = reverse_lazy("expense_categories_list")

  def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)