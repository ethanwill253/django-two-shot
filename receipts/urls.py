from django.urls import path

from receipts.views import (
  ReceiptsListView,
  ReceiptsCreateView,
  AccountsListView,
  AccountsCreateView,
  CategoriesListView,
  CategoriesCreateView,
)


urlpatterns = [
  path('', ReceiptsListView.as_view(), name='receipt_list'),
  path('create/', ReceiptsCreateView.as_view(), name='receipt_create'),
  path('accounts/', AccountsListView.as_view(), name='accounts_list'),
  path('accounts/create/', AccountsCreateView.as_view(), name='accounts_create'),
  path('expense_categories/', CategoriesListView.as_view(), name='expense_categories_list'),
  path('expense_categories/create/', CategoriesCreateView.as_view(), name='expense_categories_create'),
]
